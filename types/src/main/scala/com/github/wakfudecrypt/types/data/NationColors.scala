package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class NationColors(
  id: Int,
  color: String
)

object NationColors extends BinaryDataCompanion[NationColors] {
  override val dataId = 51
}
