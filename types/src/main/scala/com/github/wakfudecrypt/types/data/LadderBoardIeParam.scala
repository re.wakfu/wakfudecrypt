package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class LadderBoardIeParam(
  _0: Int
)

object LadderBoardIeParam extends BinaryDataCompanion[LadderBoardIeParam] {
  override val dataId = 94
}
