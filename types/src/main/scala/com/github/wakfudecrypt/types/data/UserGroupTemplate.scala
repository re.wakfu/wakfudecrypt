package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class UserGroupTemplate(
  _0: Int,
  _1: Boolean,
  _2: Map[Int, UserGroupTemplate_2]
)

@BinaryDecoder
case class UserGroupTemplate_2(
  _0: Int,
  _1: Short,
  _2: String
)

object UserGroupTemplate extends BinaryDataCompanion[UserGroupTemplate] {
  override val dataId = 74
}
