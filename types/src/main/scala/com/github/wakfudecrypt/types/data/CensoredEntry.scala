package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CensoredEntry(
  id: Int,
  deepSearch: Boolean,
  language: Int,
  censorType: Int,
  text: String
)

object CensoredEntry extends BinaryDataCompanion[CensoredEntry] {
  override val dataId = 13
}
