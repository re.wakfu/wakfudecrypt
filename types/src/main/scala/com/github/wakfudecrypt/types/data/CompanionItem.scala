package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CompanionItem(
  id: Int
)

object CompanionItem extends BinaryDataCompanion[CompanionItem] {
  override val dataId = 131
}
