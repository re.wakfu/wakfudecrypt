package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BackgroundFeedback(
  id: Int,
  `type`: Short,
  pages: Array[BackgroundFeedbackPages]
)

@BinaryDecoder
case class BackgroundFeedbackPages(
  id: Int,
  order: Short,
  template: Short,
  imageId: Int
)

object BackgroundFeedback extends BinaryDataCompanion[BackgroundFeedback] {
  override val dataId = 6
}
