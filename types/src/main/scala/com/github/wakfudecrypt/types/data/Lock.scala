package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Lock(
  id: Int,
  lockedItemId: Int,
  lockValue: Int,
  periodDuration: Long,
  unlockDate: Long,
  availableForCitizensOnly: Boolean
)

object Lock extends BinaryDataCompanion[Lock] {
  override val dataId = 120
}
