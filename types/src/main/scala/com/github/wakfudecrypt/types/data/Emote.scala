package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Emote(
  id: Int,
  `type`: Short,
  cmd: String,
  needTarget: Boolean,
  moveToTarget: Boolean,
  infiniteDuration: Boolean,
  scriptParams: Array[String],
  _7: Int,
  _8: Int,
  _9: String,
  _10: Array[Int],
  _11: Array[Int]
)

object Emote extends BinaryDataCompanion[Emote] {
  override val dataId = 81
}
