package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterGroupTemplate(
  _0: Int,
  _1: Array[Int],
  _2: Map[Int, Int],
  _3: Array[Int]
)

object MonsterGroupTemplate extends BinaryDataCompanion[MonsterGroupTemplate] {
  override val dataId = 45
}
