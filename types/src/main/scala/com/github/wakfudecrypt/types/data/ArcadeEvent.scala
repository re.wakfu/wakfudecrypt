package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ArcadeEvent(
  _0: Int,
  _1: Array[Int],
  _2: Array[Int],
  _3: Short,
  _4: String,
  _5: Short,
  _6: String
)

object ArcadeEvent extends BinaryDataCompanion[ArcadeEvent] {
  override val dataId = 88
}
