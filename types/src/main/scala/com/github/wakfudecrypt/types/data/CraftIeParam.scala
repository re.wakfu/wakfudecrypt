package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CraftIeParam(
  id: Int,
  apsId: Int,
  visualMruId: Int,
  skillId: Int,
  allowedRecipes: Array[Int],
  _5: CraftIeParam_5
)

@BinaryDecoder
case class CraftIeParam_5(
  _0: Byte,
  _1: Int
)

object CraftIeParam extends BinaryDataCompanion[CraftIeParam] {
  override val dataId = 24
}
