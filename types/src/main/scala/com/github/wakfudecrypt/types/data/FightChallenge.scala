package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class FightChallenge(
  id: Int,
  dropWeight: Int,
  dropCriterion: String,
  stateId: Int,
  listenedEffectSuccess: Int,
  listenedEffectFailure: Int,
  gfxId: Int,
  isBase: Boolean,
  incompatibleChallenges: Array[Int],
  incompatibleMonsters: Array[Int],
  rewards: Array[FightChallengeRewards]
)

@BinaryDecoder
case class FightChallengeRewards(
  id: Int,
  criterion: String,
  xpLevel: Short,
  dropLevel: Short
)

object FightChallenge extends BinaryDataCompanion[FightChallenge] {
  override val dataId = 130
}
