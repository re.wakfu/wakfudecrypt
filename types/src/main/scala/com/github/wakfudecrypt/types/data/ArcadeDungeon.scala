package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ArcadeDungeon(
  id: Int,
  worldId: Short,
  challenges: Array[ArcadeDungeonChallenges],
  rewardsList: Array[ArcadeDungeonRewardsList],
  ranks: Array[ArcadeDungeonRanks],
  scoreRoundBase: Int,
  scoreRoundIncr: Int
)

@BinaryDecoder
case class ArcadeDungeonRanks(
  levelOrder: Int,
  scoreMinD: Int,
  scoreMinC: Int,
  scoreMinB: Int,
  scoreMinA: Int
)

@BinaryDecoder
case class ArcadeDungeonRewardsListRewards(
  scoreMin: Int,
  itemId: Int,
  xpValue: Int,
  rankNeeded: Byte
)

@BinaryDecoder
case class ArcadeDungeonRewardsList(
  id: Int,
  levelOrder: Int,
  rewards: Array[ArcadeDungeonRewardsListRewards]
)

@BinaryDecoder
case class ArcadeDungeonChallenges(
  id: Int,
  ratio: Float
)

object ArcadeDungeon extends BinaryDataCompanion[ArcadeDungeon] {
  override val dataId = 87
}
