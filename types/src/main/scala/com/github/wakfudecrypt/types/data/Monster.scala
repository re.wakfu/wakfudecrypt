package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Monster(
  id: Int,
  familyId: Int,
  levelMin: Short,
  levelMax: Short,
  xpMultiplicator: Float,
  arcadePointsMultiplicator: Float,
  baseHp: Int,
  baseWp: Int,
  baseAp: Int,
  baseMp: Int,
  baseRange: Int,
  baseInit: Int,
  basePerception: Int,
  baseParade: Int,
  baseWill: Int,
  baseCriticalHit: Int,
  baseTimeBeforeDeath: Int,
  HpInc: Float,
  WpInc: Float,
  ApInc: Float,
  MpInc: Float,
  rangeInc: Float,
  initInc: Float,
  perceptionInc: Float,
  paradeInc: Float,
  willInc: Float,
  CriticalHitInc: Float,
  baseHealingBonus: Int,
  baseSummonBonus: Int,
  baseMechanicsBonus: Int,
  baseTackleBonus: Int,
  baseFireDamageBonus: Int,
  baseWaterDamageBonus: Int,
  baseEarthDamageBonus: Int,
  baseWindDamageBonus: Int,
  baseFireResistance: Int,
  baseWaterResistance: Int,
  baseEarthResistance: Int,
  baseWindResistance: Int,
  baseTackleResistance: Int,
  baseAPLossResistance: Int,
  basePMLossResistance: Int,
  baseWPLossResistance: Int,
  healingBonusInc: Float,
  tackleBonusInc: Float,
  fireDamageBonusInc: Float,
  waterDamageBonusInc: Float,
  earthDamageBonusInc: Float,
  windDamageBonusInc: Float,
  fireResistanceInc: Float,
  waterResistanceInc: Float,
  earthResistanceInc: Float,
  windResistanceInc: Float,
  tackleResistanceInc: Float,
  apLossResistanceInc: Float,
  pmLossResistanceInc: Float,
  wpLossResistanceInc: Float,
  hasDeadEvolution: Boolean,
  npcRank: Short,
  agroRadius: Short,
  sightRadius: Short,
  radiusSize: Int,
  fightProperties: Array[Int],
  worldProperties: Array[Int],
  naturalStates: Array[Short],
  spellsIdAndLevel: Array[MonsterSpellsIdAndLevel],
  familyRank: Byte,
  walkingSpeed: Short,
  runningSpeed: Short,
  runningRadiusInWorld: Short,
  runningRadiusInFight: Short,
  monsterActionData: Array[MonsterMonsterActionData],
  monsterCollectActionData: Array[MonsterMonsterCollectActionData],
  monsterBehaviourData: Array[MonsterMonsterBehaviourData],
  monsterEvolutionData: Array[MonsterMonsterEvolutionData],
  requiredLeadershipPoints: Int,
  alignmentId: Short,
  timelineBuffId: Int,
  monsterHeight: Int,
  defeatScriptId: Int,
  gfxEquipment: Array[Int],
  specialGfxEquipement: Array[MonsterSpecialGfxEquipement],
  specialGfxColor: Array[MonsterSpecialGfxColor],
  specialGfxAnim: Array[MonsterSpecialGfxAnim],
  gfx: Int,
  timelineGfx: Int,
  _86: String
)

@BinaryDecoder
case class MonsterSpecialGfxAnim(
  key: Byte,
  animName: String
)

@BinaryDecoder
case class MonsterSpecialGfxColor(
  partIndex: Int,
  color: Int
)

@BinaryDecoder
case class MonsterSpecialGfxEquipement(
  fileId: String,
  parts: Array[String]
)

@BinaryDecoder
case class MonsterMonsterEvolutionData(
  id: Int,
  breedId: Int,
  scriptId: Int
)

@BinaryDecoder
case class MonsterMonsterBehaviourData(
  id: Int,
  `type`: Int,
  scriptId: Int,
  needsToWaitPathEnd: Boolean
)

@BinaryDecoder
case class MonsterMonsterCollectActionData(
  id: Int,
  skillId: Int,
  skillLevelRequired: Int,
  skillVisualFeedbackId: Int,
  criteria: String,
  xpFactor: Float,
  collectLootListId: Int,
  duration: Int,
  collectItemId: Int,
  lootList: Array[Int],
  displayInCraftDialog: Boolean
)

@BinaryDecoder
case class MonsterMonsterActionData(
  id: Int,
  `type`: Byte,
  criteria: String,
  criteriaOnNpc: Boolean,
  moveToMonsterBeforeInteractWithHim: Boolean,
  duration: Int,
  showProgressBar: Boolean,
  visualId: Int,
  scriptId: Int
)

@BinaryDecoder
case class MonsterSpellsIdAndLevel(
  id: Int,
  level: Int
)

object Monster extends BinaryDataCompanion[Monster] {
  override val dataId = 42
}
