package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ProtectorEcosystemProtection(
  protectorId: Int,
  faunaProtection: Array[ProtectorEcosystemProtectionFaunaProtection],
  floraProtection: Array[ProtectorEcosystemProtectionFloraProtection]
)

@BinaryDecoder
case class ProtectorEcosystemProtectionFloraProtection(
  resourceFamilyId: Int,
  protectionCost: Int,
  reintroductionCost: Int,
  reintroductionItemId: Int,
  reintroductionItemQty: Short
)

@BinaryDecoder
case class ProtectorEcosystemProtectionFaunaProtection(
  monsterFamilyId: Int,
  protectionCost: Int,
  reintroductionCost: Int,
  reintroductionItemId: Int,
  reintroductionItemQty: Short
)

object ProtectorEcosystemProtection extends BinaryDataCompanion[ProtectorEcosystemProtection] {
  override val dataId = 57
}
