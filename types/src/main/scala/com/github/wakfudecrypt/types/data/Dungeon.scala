package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Dungeon(
  id: Int,
  minLevel: Short,
  instanceId: Short,
  _3: Short,
  tps: Array[Int],
  _5: Boolean,
  _6: Boolean,
  _7: Boolean,
  _8: Array[Int]
)

object Dungeon extends BinaryDataCompanion[Dungeon] {
  override val dataId = 119
}
