package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterGroupTemplateTable(
  _0: Int,
  _1: Int,
  _2: Map[Int, Float]
)

object MonsterGroupTemplateTable extends BinaryDataCompanion[MonsterGroupTemplateTable] {
  override val dataId = 46
}
