package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Zaap(
  zaapId: Int,
  exitX: Int,
  exitY: Int,
  exitWorldId: Int,
  visualId: Int,
  uiGfxId: Int,
  landmarkTravelType: Byte,
  zaapBase: Boolean,
  destinationCriteria: String,
  _9: Int,
  _10: Int,
  _11: Zaap_11
)

@BinaryDecoder
case class Zaap_11(
  _0: String,
  _1: Int,
  _2: Int,
  _3: Int
)

object Zaap extends BinaryDataCompanion[Zaap] {
  override val dataId = 76
}
