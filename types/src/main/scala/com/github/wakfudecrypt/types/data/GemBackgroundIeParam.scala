package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class GemBackgroundIeParam(
  id: Int,
  backgroundFeedback: Int,
  havreGemTypes: Array[Int]
)

object GemBackgroundIeParam extends BinaryDataCompanion[GemBackgroundIeParam] {
  override val dataId = 30
}
