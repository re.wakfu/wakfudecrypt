package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class StreetLightIeParam(
  id: Int,
  color: Int,
  range: Float,
  apsId: Int,
  activeOnlyInNight: Boolean,
  ignitionVisualId: Int,
  ignitionUseObject: Boolean,
  ignitionDuration: Int,
  extinctionVisualId: Int,
  extinctionUseObject: Boolean,
  extinctionDuration: Int,
  _11: StreetLightIeParam_11
)

@BinaryDecoder
case class StreetLightIeParam_11(
  _0: Byte,
  _1: Int
)

object StreetLightIeParam extends BinaryDataCompanion[StreetLightIeParam] {
  override val dataId = 71
}
