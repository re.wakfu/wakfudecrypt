package com.github.wakfudecrypt.types

import enumeratum.values.{ShortEnum, ShortEnumEntry}

sealed abstract class InteractiveElementType(val value: Short) extends ShortEnumEntry

case object InteractiveElementType extends ShortEnum[InteractiveElementType] {

  case object Lever extends InteractiveElementType(1)

  case object PositionTrigger extends InteractiveElementType(2)

  case object StoppingPositionTrigger extends InteractiveElementType(3)

  case object Stool extends InteractiveElementType(4)

  case object Dummy extends InteractiveElementType(5)

  case object ZoneTrigger extends InteractiveElementType(6)

  case object DimensionalBag extends InteractiveElementType(7)

  case object CraftTable extends InteractiveElementType(8)

  case object DimensionalBagMerchantConsole extends InteractiveElementType(9)

  case object Door extends InteractiveElementType(12)

  case object Destructible extends InteractiveElementType(13)

  case object Chest extends InteractiveElementType(14)

  case object FloorItem extends InteractiveElementType(15)

  case object RespawnPoint extends InteractiveElementType(16)

  case object ChallengeGenericInteractiveElement extends InteractiveElementType(17)

  case object ChallengeCraftTable extends InteractiveElementType(18)

  case object LootChest extends InteractiveElementType(19)

  case object Board extends InteractiveElementType(20)

  case object GenericStaticInteractiveElement extends InteractiveElementType(21)

  case object NPCBlocker extends InteractiveElementType(22)

  case object TicketCollector extends InteractiveElementType(23)

  case object BoatBoard extends InteractiveElementType(24)

  case object Mailbox extends InteractiveElementType(38)

  case object MerchantDisplayer extends InteractiveElementType(29)

  case object GuildMachine extends InteractiveElementType(30)

  case object BallotBox extends InteractiveElementType(31)

  case object RecycleMachine extends InteractiveElementType(32)

  case object Decoration extends InteractiveElementType(28)

  case object PortableCraftTable extends InteractiveElementType(33)

  case object DimensionalBagAdminConsole extends InteractiveElementType(34)

  case object DimensionalBagExitTrigger extends InteractiveElementType(36)

  case object DimensionalBagPermissionsConsole extends InteractiveElementType(37)

  case object DimensionalBagShelf extends InteractiveElementType(35)

  case object GenericActivableInteractiveElement extends InteractiveElementType(39)

  case object MarketBoard extends InteractiveElementType(40)

  case object WeatherBoard extends InteractiveElementType(41)

  case object StoppingPositionZoneTrigger extends InteractiveElementType(42)

  case object DimensionalBagBackgroundDisplay extends InteractiveElementType(43)

  case object DimensionalBagLock extends InteractiveElementType(44)

  case object StreetLight extends InteractiveElementType(45)

  case object BackgroundDisplay extends InteractiveElementType(46)

  case object Zaap extends InteractiveElementType(26)

  case object Drago extends InteractiveElementType(47)

  case object Boat extends InteractiveElementType(48)

  case object Cannon extends InteractiveElementType(49)

  case object Collector extends InteractiveElementType(50)

  case object Teleporter extends InteractiveElementType(51)

  case object AudioMarker extends InteractiveElementType(52)

  case object DungeonLadderBoard extends InteractiveElementType(53)

  case object DungeonLadderStatue extends InteractiveElementType(54)

  case object Bed extends InteractiveElementType(55)

  case object StorageBox extends InteractiveElementType(56)

  case object CoinMachine extends InteractiveElementType(57)

  case object DialogMachine extends InteractiveElementType(58)

  case object ExchangeMachine extends InteractiveElementType(59)

  case object GuildStorageBox extends InteractiveElementType(60)

  case object GuildLadder extends InteractiveElementType(61)

  case object DungeonDisplayer extends InteractiveElementType(62)

  case object RewardDisplayer extends InteractiveElementType(63)

  case object HavenWorldBoard extends InteractiveElementType(64)

  case object EquipableDummy extends InteractiveElementType(65)

  case object Bookcase extends InteractiveElementType(66)

  case object HavenWorldBuildingBoard extends InteractiveElementType(67)

  case object KrosmozGameBoard extends InteractiveElementType(68)

  case object KrosmozGameCollection extends InteractiveElementType(69)

  case object ZaapOutOnly extends InteractiveElementType(70)

  case object HavenWorldDrago extends InteractiveElementType(71)

  case object HavenWorldExit extends InteractiveElementType(72)

  case object HavenWorldResourcesCollector extends InteractiveElementType(73)

  case object SeedSpreader extends InteractiveElementType(74)

  case object NationSelectionBoard extends InteractiveElementType(75)

  val values = findValues
}
