package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Aptitude(
  id: Int,
  breedId: Short,
  characteristicId: Byte,
  uiId: Int,
  aptitudeGfxId: Int,
  spellGfxId: Int,
  maxLevel: Short,
  constantCost: Int,
  variableCost: Array[Int],
  linkedSpellId: Int,
  levelUnlock: Array[Int],
  effectIds: Array[Int]
)

object Aptitude extends BinaryDataCompanion[Aptitude] {
  override val dataId = 78
}
