package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class FixedAbilityCategory(
  categoryId: Int,
  levels: Array[Int],
  bonusIds: Array[Int]
)

object FixedAbilityCategory extends BinaryDataCompanion[FixedAbilityCategory] {
  override val dataId = 136
}
