package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldBuildingVisualDefinition(
  id: Int,
  buildingId: Int,
  elements: Array[HavenWorldBuildingVisualDefinitionElements]
)

@BinaryDecoder
case class HavenWorldBuildingVisualDefinitionElements(
  uid: Int,
  gfxId: Int,
  hasGuildColor: Boolean,
  occluder: Boolean,
  height: Byte,
  animName: String,
  direction: Byte,
  x: Byte,
  y: Byte,
  z: Byte
)

object HavenWorldBuildingVisualDefinition extends BinaryDataCompanion[HavenWorldBuildingVisualDefinition] {
  override val dataId = 107
}
