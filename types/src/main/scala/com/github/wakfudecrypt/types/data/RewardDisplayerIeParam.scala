package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class RewardDisplayerIeParam(
  id: Int,
  itemIds: Array[Int]
)

object RewardDisplayerIeParam extends BinaryDataCompanion[RewardDisplayerIeParam] {
  override val dataId = 96
}
