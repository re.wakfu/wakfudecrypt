package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AchievementCategory(
  id: Int,
  parentId: Int
)

object AchievementCategory extends BinaryDataCompanion[AchievementCategory] {
  override val dataId = 3
}
