package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Nation(
  _0: Int,
  _1: Short,
  _2: Short,
  _3: Short,
  _4: Short,
  _5: Int,
  _6: Int,
  _7: Short,
  _8: Int,
  _9: Int
)

object Nation extends BinaryDataCompanion[Nation] {
  override val dataId = 50
}
