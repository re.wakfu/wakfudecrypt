package com.github.wakfudecrypt.types

import enumeratum.values.{IntEnum, IntEnumEntry}

sealed abstract class ActionType(val value: Int) extends IntEnumEntry

case object ActionType extends IntEnum[ActionType] {

  case object HpLoss extends ActionType(1)

  case object HpFireLoss extends ActionType(2)

  case object HpEarthLoss extends ActionType(3)

  case object HpWaterLoss extends ActionType(4)

  case object HpAirLoss extends ActionType(5)

  case object HpLossWithKamasWater extends ActionType(6)

  case object HpLeech extends ActionType(11)

  case object HpLeechFire extends ActionType(12)

  case object HpLeechEarth extends ActionType(13)

  case object HpLeechWater extends ActionType(14)

  case object HpLeechAir extends ActionType(15)

  case object SetHp extends ActionType(16)

  case object HpGainWithFeedback extends ActionType(17)

  case object HpBoost extends ActionType(20)

  case object HpDeboost extends ActionType(21)

  case object HpLossAccumulation extends ActionType(23)

  case object CopyCharacMax extends ActionType(24)

  case object HpGain extends ActionType(25)

  case object HealGain extends ActionType(26)

  case object HealLoss extends ActionType(27)

  case object Death extends ActionType(28)

  case object HpGainPercentOfValue extends ActionType(29)

  case object ActionCost extends ActionType(30)

  case object ApBoost extends ActionType(31)

  case object ApDeboost extends ActionType(32)

  case object ApGain extends ActionType(33)

  case object ApLeech extends ActionType(35)

  case object ApGift extends ActionType(36)

  case object ApLoss extends ActionType(37)

  case object ResApGain extends ActionType(38)

  case object CharacGain extends ActionType(39)

  case object CharacLoss extends ActionType(40)

  case object MpBoost extends ActionType(41)

  case object MpDeboost extends ActionType(42)

  case object MpGain extends ActionType(43)

  case object MpLoss extends ActionType(44)

  case object MpLeech extends ActionType(45)

  case object ResMpGain extends ActionType(46)

  case object SpellBoostLevel extends ActionType(47)

  case object HpLossWithRebound extends ActionType(50)

  case object HpEarthLossWithRebound extends ActionType(51)

  case object HpFireLossWithRebound extends ActionType(52)

  case object HpWaterLossWithRebound extends ActionType(53)

  case object HpAirLossWithRebound extends ActionType(54)

  case object HpExchange extends ActionType(59)

  case object HpGainWithReboundWater extends ActionType(60)

  case object CharacStealWithLoss extends ActionType(61)

  case object ApLeechInPercent extends ActionType(62)

  case object WpLeechInPercent extends ActionType(63)

  case object DmgLeechInPercent extends ActionType(64)

  case object ResLeechInPercent extends ActionType(65)

  case object MpLeechInPercent extends ActionType(66)

  case object HpGainWithReboundAir extends ActionType(67)

  case object HealResistGain extends ActionType(70)

  case object RearResistGain extends ActionType(71)

  case object HealResistLoss extends ActionType(72)

  case object WpLeech extends ActionType(73)

  case object InitLeech extends ActionType(74)

  case object GeneralResistGain extends ActionType(80)

  case object FireResistGain extends ActionType(82)

  case object WaterResistGain extends ActionType(83)

  case object EarthResistGain extends ActionType(84)

  case object AirResistGain extends ActionType(85)

  case object GeneralResistLoss extends ActionType(90)

  case object EarthResistLoss extends ActionType(92)

  case object FireResistLoss extends ActionType(93)

  case object WaterResistLoss extends ActionType(94)

  case object AirResistLoss extends ActionType(95)

  case object GeneralMasteryGain extends ActionType(120)

  case object FireMasteryGain extends ActionType(122)

  case object EarthMasteryGain extends ActionType(123)

  case object WaterMasteryGain extends ActionType(124)

  case object AirMasteryGain extends ActionType(125)

  case object DamageInflictedGain extends ActionType(126)

  case object DamageTakenLoss extends ActionType(127)

  case object DamageTakenGain extends ActionType(128)

  case object DamageInflictedLoss extends ActionType(129)

  case object GeneralMasteryLoss extends ActionType(130)

  case object FireMasteryLoss extends ActionType(132)

  case object EarthMasteryLoss extends ActionType(133)

  case object WaterMasteryLoss extends ActionType(134)

  case object AirMasteryLoss extends ActionType(135)

  case object Punishment extends ActionType(136)

  case object DamagesReboundInPercent extends ActionType(140)

  case object DamagesAbsorbInPercent extends ActionType(141)

  case object LifeStolenBonusGain extends ActionType(148)

  case object CriticalMasteryGain extends ActionType(149)

  case object CriticalHitGain extends ActionType(150)

  case object CriticalFailureGain extends ActionType(151)

  case object AgilityGain extends ActionType(152)

  case object AgilityLoss extends ActionType(153)

  case object StrengthGain extends ActionType(154)

  case object StrengthLoss extends ActionType(155)

  case object IntelligenceGain extends ActionType(156)

  case object IntelligenceLoss extends ActionType(157)

  case object LuckGain extends ActionType(158)

  case object LuckLoss extends ActionType(159)

  case object RangeGain extends ActionType(160)

  case object RangeLoss extends ActionType(161)

  case object ProspectingGain extends ActionType(162)

  case object ProspectingLoss extends ActionType(163)

  case object WisdomGain extends ActionType(166)

  case object WisdomLoss extends ActionType(167)

  case object CriticalHitLoss extends ActionType(168)

  case object CriticalFailureLoss extends ActionType(169)

  case object InitiativeGain extends ActionType(171)

  case object InitiativeLoss extends ActionType(172)

  case object LockGain extends ActionType(173)

  case object LockLoss extends ActionType(174)

  case object DodgeGain extends ActionType(175)

  case object DodgeLoss extends ActionType(176)

  case object RearMasteryGain extends ActionType(180)

  case object RearMasteryLoss extends ActionType(181)

  case object MechanicsMastery extends ActionType(182)

  case object MechanicsGain extends ActionType(183)

  case object ControlGain extends ActionType(184)

  case object WpBoost extends ActionType(191)

  case object WpDeboost extends ActionType(192)

  case object WpGain extends ActionType(193)

  case object WpLoss extends ActionType(194)

  case object WpSteal extends ActionType(195)

  case object Raise extends ActionType(196)

  case object VitalityGain extends ActionType(197)

  case object VitalityLoss extends ActionType(198)

  case object SkipTurn extends ActionType(200)

  case object ReduceRangeToCac extends ActionType(201)

  case object Rooted extends ActionType(202)

  case object ImmuneToMpLoss extends ActionType(203)

  case object Invisible extends ActionType(204)

  case object InvisibleSuperior extends ActionType(205)

  case object FightForbidden extends ActionType(206)

  case object ChangeController extends ActionType(207)

  case object Coward extends ActionType(208)

  case object Haste extends ActionType(209)

  case object Stabilized extends ActionType(210)

  case object CantBeAttacked extends ActionType(211)

  case object CantTackle extends ActionType(212)

  case object StateResistance extends ActionType(213)

  case object NoFightXp extends ActionType(214)

  case object SetMaxApCos extends ActionType(215)

  case object SetEffectAreaGrip extends ActionType(216)

  case object RevealInvisible extends ActionType(217)

  case object Undead extends ActionType(218)

  case object Untargettable extends ActionType(219)

  case object IaDoubleFocus extends ActionType(220)

  case object Stasis1 extends ActionType(221)

  case object Stasis2 extends ActionType(222)

  case object Stasis3 extends ActionType(223)

  case object IaMinimizeFocus extends ActionType(224)

  case object RaiseOutOfCombat extends ActionType(225)

  case object Groggy1 extends ActionType(226)

  case object Groggy2 extends ActionType(227)

  case object Groggy3 extends ActionType(228)

  case object Crippled1 extends ActionType(229)

  case object Crippled2 extends ActionType(230)

  case object Crippled3 extends ActionType(231)

  case object Lame extends ActionType(232)

  case object KitSkillGain extends ActionType(234)

  case object ApRemovalChanceGain extends ActionType(235)

  case object MpRemovalChanceGain extends ActionType(236)

  case object ExtraTurn extends ActionType(237)

  case object NoDeath extends ActionType(238)

  case object StateApplicationBonus extends ActionType(239)

  case object DontBlockLos extends ActionType(240)

  case object EscapeTackle extends ActionType(241)

  case object CantTeleport extends ActionType(242)

  case object HpBoostInPercentFunctionAlliesCount extends ActionType(249)

  case object HpBoostInPercent extends ActionType(250)

  case object HpGainAir extends ActionType(251)

  case object HpGainEarth extends ActionType(253)

  case object HpGainWater extends ActionType(258)

  case object HpGainFire extends ActionType(259)

  case object WisdomBoost extends ActionType(260)

  case object HpGainAirForCasterBasedOnTarget extends ActionType(261)

  case object HpGainEarthForCasterBasedOnTarget extends ActionType(262)

  case object HpGainWaterForCasterBasedOnTarget extends ActionType(263)

  case object HpGainFireForCasterBasedOnTarget extends ActionType(264)

  case object HpGainPhysicalForCasterBasedOnTarget extends ActionType(265)

  case object InitGainPercentOfValue extends ActionType(270)

  case object InitLossPercentOfValue extends ActionType(271)

  case object MpSetToMax extends ActionType(272)

  case object Push extends ActionType(300)

  case object Pull extends ActionType(301)

  case object TeleportCaster extends ActionType(302)

  case object AttractSight extends ActionType(303)

  case object StateApply extends ActionType(304)

  case object GetCloser extends ActionType(305)

  case object PullOnCasterBack extends ActionType(306)

  case object Summon extends ActionType(307)

  case object SetTrap extends ActionType(308)

  case object SpellRebound extends ActionType(309)

  case object RunningState extends ActionType(310)

  case object SetCadran extends ActionType(311)

  case object SetHour extends ActionType(312)

  case object SetBeacon extends ActionType(313)

  case object SetAura extends ActionType(314)

  case object SetGlyph extends ActionType(315)

  case object UnsetAuras extends ActionType(316)

  case object SetBomb extends ActionType(317)

  case object SeduceMonster extends ActionType(320)

  case object StateForceUnapply extends ActionType(321)

  case object SummonSramDouble extends ActionType(322)

  case object BringBack extends ActionType(323)

  case object SummonFromSymbiot extends ActionType(324)

  case object SetAi extends ActionType(325)

  case object WeaponAttack extends ActionType(326)

  case object AiGiveOrder extends ActionType(327)

  case object AiGiveOrderCell extends ActionType(328)

  case object RunningEffectGroup extends ActionType(330)

  case object Repell extends ActionType(331)

  case object Unsummon extends ActionType(332)

  case object Throw extends ActionType(333)

  case object Carry extends ActionType(334)

  case object UpdateMaxSeducableCreatures extends ActionType(337)

  case object SetAuraOnTarget extends ActionType(338)

  case object SetEffectArea extends ActionType(339)

  case object TeleportTarget extends ActionType(340)

  case object RandomRunningEffectGroup extends ActionType(341)

  case object MakeFlee extends ActionType(342)

  case object ApplyStateFunctionPaPmPw extends ActionType(343)

  case object SpellBreak extends ActionType(344)

  case object RunningEffectGroupRandomTargets extends ActionType(345)

  case object ApplyDeathtag extends ActionType(346)

  case object SummonImage extends ActionType(347)

  case object SetAreaWithLevelFunctionSpell extends ActionType(348)

  case object EffectRandomInArea extends ActionType(349)

  case object ApplyStateWithoutNotification extends ActionType(350)

  case object StateDecurseWithoutNotification extends ActionType(351)

  case object ApplyStateFunctionActionCost extends ActionType(352)

  case object ApplyStateForFecaArmor extends ActionType(353)

  case object RemoveFecaArmor extends ActionType(354)

  case object SetFecaGlyph extends ActionType(355)

  case object XelorsDialChargeGain extends ActionType(356)

  case object XelorsDialChargeLoss extends ActionType(357)

  case object SetAiWithSpellsForIaServer extends ActionType(358)

  case object NullEffect extends ActionType(400)

  case object SplitTriggerEffect extends ActionType(401)

  case object NullEffectOnCell extends ActionType(402)

  case object NullEffectNotifiedToAi extends ActionType(403)

  case object HpLossInPercentMax extends ActionType(411)

  case object HpFireLossInPercentMax extends ActionType(412)

  case object HpEarthLossInPercentMax extends ActionType(413)

  case object HpWaterLossInPercentMax extends ActionType(414)

  case object HpAirLossInPercentMax extends ActionType(415)

  case object HpLossInPercentCurrent extends ActionType(416)

  case object HpFireLossInPercentCurrent extends ActionType(417)

  case object HpEarthLossInPercentCurrent extends ActionType(418)

  case object HpWaterLossInPercentCurrent extends ActionType(419)

  case object HpAirLossInPercentCurrent extends ActionType(420)

  case object Decurse extends ActionType(421)

  case object ExchangePosition extends ActionType(422)

  case object SpellAttack extends ActionType(426)

  case object UpdateValue extends ActionType(430)

  case object KamaSteal extends ActionType(431)

  case object PickUpItem extends ActionType(432)

  case object BoostSkillWithSpell extends ActionType(433)

  case object KamaShield extends ActionType(434)

  case object UpdateTargetWithCarrier extends ActionType(435)

  case object VirtualArmor extends ActionType(436)

  case object RevealInvisibility extends ActionType(438)

  case object PoisonAp extends ActionType(440)

  case object PoisonMp extends ActionType(441)

  case object PoisonWp extends ActionType(442)

  case object UpdateCasterWithTarget extends ActionType(443)

  case object Confused extends ActionType(444)

  case object ImmuneToEffects extends ActionType(445)

  case object RemoveFightProperty extends ActionType(446)

  case object ApplyFightProperty extends ActionType(447)

  case object PoisonTrigerringValue extends ActionType(448)

  case object ApplyWorldProperty extends ActionType(449)

  case object PlayEmote extends ActionType(550)

  case object CharacSetDmgAir extends ActionType(560)

  case object CharacSetDmgEarth extends ActionType(561)

  case object CharacSetDmgWater extends ActionType(562)

  case object CharacSetDmgFire extends ActionType(563)

  case object HealBoostPercentFunctionDmgBonus extends ActionType(564)

  case object HpBoostFunctionDmgBonus extends ActionType(565)

  case object CharacSetDmgInPercent extends ActionType(566)

  case object SetOutFightHpRegen extends ActionType(567)

  case object CharacSetResAir extends ActionType(568)

  case object CharacSetResEarth extends ActionType(569)

  case object CharacSetResWater extends ActionType(570)

  case object CharacSetResFire extends ActionType(571)

  case object CharacSetResInPercent extends ActionType(572)

  case object CharacSetHealInPercent extends ActionType(573)

  case object CharacSetHp extends ActionType(574)

  case object CharacSetApDbuffPower extends ActionType(575)

  case object HpLossInPercentMaxCaster extends ActionType(620)

  case object HpFireLossInPercentMaxCaster extends ActionType(621)

  case object HpEarthLossInPercentMaxCaster extends ActionType(622)

  case object HpWaterLossInPercentMaxCaster extends ActionType(623)

  case object HpAirLossInPercentMaxCaster extends ActionType(624)

  case object HpLossInPercentCurrentCaster extends ActionType(625)

  case object HpFireLossInPercentCurrentCaster extends ActionType(626)

  case object HpEarthLossInPercentCurrentCaster extends ActionType(627)

  case object HpWaterLossInPercentCurrentCaster extends ActionType(628)

  case object HpAirLossInPercentCurrentCaster extends ActionType(629)

  case object HpLossInPercentCurrentCasterLifeLost extends ActionType(630)

  case object HpFireLossInPercentCurrentCasterLifeLost extends ActionType(631)

  case object HpEarthLossInPercentCurrentCasterLifeLost extends ActionType(632)

  case object HpWaterLossInPercentCurrentCasterLifeLost extends ActionType(633)

  case object HpAirLossInPercentCurrentCasterLifeLost extends ActionType(634)

  case object RandomHpAirLoss extends ActionType(635)

  case object RandomHpEarthLoss extends ActionType(636)

  case object RandomHpFireLoss extends ActionType(637)

  case object RandomHpWaterLoss extends ActionType(638)

  case object HpLossFunctionPaPm extends ActionType(639)

  case object AddSpellToInventory extends ActionType(640)

  case object InitLossFunctionPaPm extends ActionType(641)

  case object HpLossInPercentCurrentTargetLifeLost extends ActionType(642)

  case object HpFireLossInPercentCurrentTargetLifeLost extends ActionType(643)

  case object HpEarthLossInPercentCurrentTargetLifeLost extends ActionType(644)

  case object HpWaterLossInPercentCurrentTargetLifeLost extends ActionType(645)

  case object HpAirLossInPercentCurrentTargetLifeLost extends ActionType(646)

  case object HpLossFunctionPaPmWithoutConsumeEarth extends ActionType(647)

  case object HpLossFunctionPaPmWithoutConsumeFire extends ActionType(648)

  case object ResourcefulnessGain extends ActionType(700)

  case object CraftQuicknessGain extends ActionType(702)

  case object QuickLearnerHarvestGain extends ActionType(705)

  case object QuickLearnerCraftGain extends ActionType(706)

  case object GreenThumbsGain extends ActionType(707)

  case object EcosystemSkillModification extends ActionType(708)

  case object CraftSkillModification extends ActionType(709)

  case object ResourcefulnessLoss extends ActionType(710)

  case object GetTitle extends ActionType(800)

  case object HpLossAndPuppetHeal extends ActionType(801)

  case object LinkCasterAndTarget extends ActionType(802)

  case object ChangeSpellTargetCell extends ActionType(803)

  case object SetChangeSpellTargetCellArea extends ActionType(804)

  case object EnutrofDepositPlacement extends ActionType(805)

  case object RemoveOwnDeposit extends ActionType(807)

  case object ApAsMp extends ActionType(808)

  case object FakeKo extends ActionType(809)

  case object HpLossNormalDistribEarth extends ActionType(810)

  case object CharacGainBasedOnOtherCharacValue extends ActionType(812)

  case object SetLootEffectArea extends ActionType(813)

  case object DropFromLootArea extends ActionType(814)

  case object HpLossFromLootArea extends ActionType(815)

  case object CollectForController extends ActionType(816)

  case object ReplaceAreaByAnother extends ActionType(817)

  case object HpLossFunctionLoot extends ActionType(818)

  case object ChrageBuff extends ActionType(819)

  case object HpLossAfterMovement extends ActionType(825)

  case object DropForEnutrofBlessing extends ActionType(828)

  case object InstantKo extends ActionType(830)

  case object BlitzkriekEffect extends ActionType(831)

  case object ElementSpellGain extends ActionType(832)

  case object DoubleOrQuits extends ActionType(833)

  case object BoostVoodoolDmg extends ActionType(834)

  case object VoodoolSplitEffect extends ActionType(835)

  case object VoodoolBoostEffect extends ActionType(836)

  case object HpGainRandomPercentOfValue extends ActionType(837)

  case object SummoningMastery extends ActionType(838)

  case object SummonDoubleForBellaphone extends ActionType(839)

  case object BombCooldownDecrease extends ActionType(840)

  case object BombCooldownBuff extends ActionType(841)

  case object SetBarrel extends ActionType(842)

  case object RemoveAreaUsingTarget extends ActionType(843)

  case object RunningEffectGroupLevelFunctionState extends ActionType(844)

  case object StateGenericApplicationBonus extends ActionType(845)

  case object StateResistanceBonus extends ActionType(846)

  case object HpLossThenHpLossFunctionPreviousValueEarth extends ActionType(847)

  case object ArmorPlateBonusGain extends ActionType(849)

  case object FecaGlyphChargeBonusGain extends ActionType(850)

  case object HpLossFunctionCharac extends ActionType(851)

  case object InvertDmgAndRes extends ActionType(852)

  case object HpLossFunctionTriggeringValue extends ActionType(853)

  case object ApplyCantBePushOrPulled extends ActionType(854)

  case object RunningEffectGroupWithAtLeastNullEffect extends ActionType(855)

  case object PmTransferToTargets extends ActionType(856)

  case object TackleTransferToTargets extends ActionType(857)

  case object DodgeTransferToTargets extends ActionType(858)

  case object BringBackToOwnerSymbiot extends ActionType(859)

  case object ChangeLevel extends ActionType(860)

  case object OsaInitGain extends ActionType(861)

  case object OsaCcGain extends ActionType(862)

  case object OsaDodgeGain extends ActionType(863)

  case object OsaTackleGain extends ActionType(864)

  case object GroupSecondValueFunctionFirst extends ActionType(865)

  case object PerceptionGain extends ActionType(866)

  case object PerceptionLoss extends ActionType(867)

  case object FinalDmgGainFunctionStateLevel extends ActionType(868)

  case object OsaDmgGain extends ActionType(869)

  case object OsaApGain extends ActionType(870)

  case object OsaMpGain extends ActionType(871)

  case object WillpowerGain extends ActionType(872)

  case object WillpowerLoss extends ActionType(873)

  case object OsaRangeGain extends ActionType(874)

  case object BlockGain extends ActionType(875)

  case object BlockLoss extends ActionType(876)

  case object HpMinModification extends ActionType(877)

  case object MoveAwayFromCell extends ActionType(878)

  case object VirtualArmorBonusGain extends ActionType(879)

  case object AreaHpLoss extends ActionType(880)

  case object EffectValueFunctionFecaArmor extends ActionType(881)

  case object EffectValueFunctionGlyphCharge extends ActionType(882)

  case object ResApLoss extends ActionType(884)

  case object ResMpLoss extends ActionType(885)

  case object RunningEffectGroupProbaFunctionPaPmPw extends ActionType(886)

  case object EffectProbaFunctionGlyphCharge extends ActionType(887)

  case object EffectProbaFunctionArmorPlate extends ActionType(892)

  case object ModifySubEffectByChrage extends ActionType(894)

  case object ModifySubEffectByTargetPlate extends ActionType(895)

  case object RandomInAreaByAreaHp extends ActionType(896)

  case object ApplyStatePercentFunctionAreaHp extends ActionType(897)

  case object DmgWaterModifiedByArmorPlate extends ActionType(900)

  case object DmgAirModifiedByArmorPlate extends ActionType(901)

  case object DmgEarthModifiedByArmorPlate extends ActionType(902)

  case object DmgFireModifiedByArmorPlate extends ActionType(903)

  case object RunningEffectGroupProbaFunctionStateLevel extends ActionType(904)

  case object ModifySubEffectByArmorPlate extends ActionType(905)

  case object RegExecutionCountFunctionAreaHp extends ActionType(906)

  case object AreaHpBuff extends ActionType(907)

  case object ModifyDmgInPercentFuntionProspection extends ActionType(908)

  case object ReplaceAreaByAnotherUsingTarget extends ActionType(909)

  case object RunningEffectGroupLevelFunctionTriggeringSpellCost extends ActionType(910)

  case object RemoveAreaUsingTargetCell extends ActionType(911)

  case object RunningEffectGroupLevelFunctionTriggeringActionCost extends ActionType(912)

  case object RunningEffectGroupLevelFunctionCharacteristic extends ActionType(913)

  case object BombCooldownLossFunctionPaPm extends ActionType(914)

  case object ExecuteTriggerInEffectZone extends ActionType(915)

  case object ModifyDmgInPercentFunctionInitiative extends ActionType(916)

  case object HpStasisLoss extends ActionType(917)

  case object DmgStasisGainPercent extends ActionType(918)

  case object RollbackTriggeringSpellApCost extends ActionType(919)

  case object RollbackTriggeringSpellMpCost extends ActionType(920)

  case object RollbackTriggeringSpellWpCost extends ActionType(921)

  case object HpLossPercentOfValue extends ActionType(922)

  case object ExecuteActionCost extends ActionType(923)

  case object CharacSetAreaHp extends ActionType(924)

  case object AreaHpGain extends ActionType(925)

  case object InitBoost extends ActionType(926)

  case object ResStasisLossPercent extends ActionType(927)

  case object CharacBuffBasedOnOtherCharacValue extends ActionType(928)

  case object CharacSetAp extends ActionType(929)

  case object CharacSetInit extends ActionType(930)

  case object ReplaceTargetByOwner extends ActionType(931)

  case object DecurseLinkedToCaster extends ActionType(932)

  case object ChangePlayerSpellsByMonsterOnes extends ActionType(933)

  case object ExcludeFromFight extends ActionType(934)

  case object RemoveAllAreasWithID extends ActionType(935)

  case object SpawnMonster extends ActionType(936)

  case object RemoveFromFightMap extends ActionType(937)

  case object RegExecutionCountFunctionTriggeringActionCost extends ActionType(938)

  case object HpLossStasisExponentialGrowth extends ActionType(939)

  case object HpLossFireExponentialGrowth extends ActionType(940)

  case object HpLossEarthExponentialGrowth extends ActionType(941)

  case object HpLossAirExponentialGrowth extends ActionType(942)

  case object HpLossWaterExponentialGrowth extends ActionType(943)

  case object ResWaterModifiedByArmorPlate extends ActionType(944)

  case object ResAirModifiedByArmorPlate extends ActionType(945)

  case object ResEarthModifiedByArmorPlate extends ActionType(946)

  case object ResFireModifiedByArmorPlate extends ActionType(947)

  case object SetChrageToDmgRatio extends ActionType(948)

  case object RegExecutionCountFixed extends ActionType(949)

  case object SetAp extends ActionType(950)

  case object SetMp extends ActionType(951)

  case object SetWp extends ActionType(952)

  case object DebuffFumbleRate extends ActionType(953)

  case object SetMicrobotRailsMaxDistance extends ActionType(954)

  case object VirtualHpBoostInPercentFunctionAlliesCount extends ActionType(955)

  case object VirtualHpBoostInPercent extends ActionType(956)

  case object SetVirtualHpFromHp extends ActionType(957)

  case object HpLossInPercentMaxWithVirtual extends ActionType(958)

  case object HpLossInPercentMaxCasterWithVirtual extends ActionType(959)

  case object HpLossFireFunctionFighterLevel extends ActionType(960)

  case object HpLossEarthFunctionFighterLevel extends ActionType(961)

  case object HpLossWaterFunctionFighterLevel extends ActionType(962)

  case object HpLossAirFunctionFighterLevel extends ActionType(963)

  case object HpLossStasisFunctionFighterLevel extends ActionType(964)

  case object AddSpellToTemporarySpellInventory extends ActionType(965)

  case object PlantationBonusPlant extends ActionType(966)

  case object PlantationBonusTree extends ActionType(967)

  case object PlantationBonusCultivation extends ActionType(968)

  case object PlantationBonusMob extends ActionType(969)

  case object CraftCraftXpBonus extends ActionType(970)

  case object CollectBonusPlant extends ActionType(971)

  case object CollectBonusTree extends ActionType(972)

  case object CollectBonusCultivation extends ActionType(973)

  case object CollectBonusMob extends ActionType(974)

  case object CollectBonusMineral extends ActionType(975)

  case object CollectBonusFish extends ActionType(976)

  case object CraftQuicknessBonus extends ActionType(977)

  case object PlantationBonusTreasure extends ActionType(978)

  case object AllElementalSpellGain extends ActionType(979)

  case object PassiveSpellGain extends ActionType(980)

  case object ActiveSupportSpellGain extends ActionType(981)

  case object RemovePassiveSpells extends ActionType(982)

  case object ApSetToMax extends ActionType(983)

  case object SetVitualHp extends ActionType(984)

  case object VirtualHpSetToMax extends ActionType(985)

  case object UpdateHpMaxPercentModifier extends ActionType(986)

  case object AddBonusLoot extends ActionType(987)

  case object CriticalResGain extends ActionType(988)

  case object UpdateVirtualHpMaxPercentModifier extends ActionType(989)

  case object HpGainForUndead extends ActionType(990)

  case object KoTimeBeforeDeathBuff extends ActionType(991)

  case object TeleportCasterBehindTarget extends ActionType(992)

  case object ChangeSpellTargetCellByCasterCell extends ActionType(993)

  case object SummonDoubleForIceStatue extends ActionType(994)

  case object RunningEffectGroupLevelFunctionSpell extends ActionType(995)

  case object OsaInvocationKnowledgeGain extends ActionType(996)

  case object VirtualArmorFlat extends ActionType(997)

  case object DmgWaterPercentGainFctCasterDmgEarthPercent extends ActionType(998)

  case object RunningEffectGroupLevelFunctionCharacterLevel extends ActionType(999)

  case object ChangeTrigerringEffectTarget extends ActionType(1000)

  case object CopyCharacAtApplicationValue extends ActionType(1001)

  case object SummonZobalDouble extends ActionType(1002)

  case object KoTimeBeforeDeathMinModification extends ActionType(1003)

  case object ModifyDmgInPercentFunctionDodge extends ActionType(1004)

  case object ApplyStateCapedByAnotherState extends ActionType(1005)

  case object RegSubEffectValueFunctionCasterAp extends ActionType(1006)

  case object RegSubEffectValueFunctionCasterMp extends ActionType(1007)

  case object SetEffectAreaForOriginalController extends ActionType(1008)

  case object ModifyHealBonusInPercentFunctionDodge extends ActionType(1009)

  case object ArmorGain extends ActionType(1010)

  case object ArmorLoss extends ActionType(1011)

  case object ArmorElementalGain extends ActionType(1012)

  case object ReduceZoneEffect extends ActionType(1013)

  case object CopyCharacInRealTime extends ActionType(1014)

  case object BestElementHpLoss extends ActionType(1015)

  case object BestElementHpGain extends ActionType(1016)

  case object BestElementHpGainFunctionPaPm extends ActionType(1017)

  case object SpecificForArmorDamageRebound extends ActionType(1018)

  case object PvpBuffGain extends ActionType(1019)

  case object RunningEffectGroupLevelFunctionTriggeringValue extends ActionType(1020)

  case object RemoveBlockingAreaUsingTarget extends ActionType(1021)

  case object CraftEcosystemXpBonus extends ActionType(1022)

  case object CraftXpBonus extends ActionType(1023)

  case object StateDecurseForHyperaction extends ActionType(1024)

  case object HpGainFunctionCasterEarth extends ActionType(1025)

  case object HpGainFunctionCasterAir extends ActionType(1026)

  case object HpGainFunctionCasterWater extends ActionType(1027)

  case object HpGainFunctionCasterFire extends ActionType(1028)

  case object HpGainFunctionCasterPhysical extends ActionType(1029)

  case object SetTeamID extends ActionType(1030)

  case object RemoveEquipmentEffects extends ActionType(1031)

  case object RemoveSetEffects extends ActionType(1032)

  case object SetSteamerBlock extends ActionType(1033)

  case object SteamerBlockPassiveResist extends ActionType(1034)

  case object SteamerBlockDamageRedirection extends ActionType(1035)

  case object WeakestResistGain extends ActionType(1036)

  case object SteamerDamageRedirection extends ActionType(1037)

  case object AddCharacValueToAnotherInRealTime extends ActionType(1038)

  case object CharacSetControl extends ActionType(1039)

  case object SetResistanceTarget extends ActionType(1040)

  case object RegExecutionCountFunctionAlliesCount extends ActionType(1041)

  case object BarrierElementalGain extends ActionType(1042)

  case object UpdateTackleHpMaxPercentModifier extends ActionType(1043)

  case object ChangeAreaPosition extends ActionType(1044)

  case object ModifyResInPercentFunctionTackle extends ActionType(1045)

  case object AddToAiPriorityTargets extends ActionType(1046)

  case object RunningEffectGroupLevelFunctionTotalHp extends ActionType(1047)

  case object DivideHpLossUnlessThreshold extends ActionType(1048)

  case object SwitchCharacValues extends ActionType(1049)

  case object AoeMasteryGain extends ActionType(1050)

  case object SingleTargetMasteryGain extends ActionType(1051)

  case object MeleeMasteryGain extends ActionType(1052)

  case object DistanceMasteryGain extends ActionType(1053)

  case object NonCritMasteryGain extends ActionType(1054)

  case object BerserkMasteryGain extends ActionType(1055)

  case object CritMasteryLoss extends ActionType(1056)

  case object SingleTargetMasteryLoss extends ActionType(1057)

  case object AoeMasteryLoss extends ActionType(1058)

  case object MeleeMasteryLoss extends ActionType(1059)

  case object DistanceMasteryLoss extends ActionType(1060)

  case object BerserkMasteryLoss extends ActionType(1061)

  case object CriticalResistLoss extends ActionType(1062)

  case object RearResistLoss extends ActionType(1063)

  case object ControlLoss extends ActionType(1064)

  case object SpellWithPropertyApCostReduction extends ActionType(1065)

  case object SpellWithPropertyRangeGain extends ActionType(1066)

  case object TackleActionCost extends ActionType(1067)

  case object VariableElementsMasteryGain extends ActionType(1068)

  case object VariableElementsResGain extends ActionType(1069)

  case object AddCharacValueToAnotherInRealTimeWithThreshold extends ActionType(1070)

  case object VirtualHpBoost extends ActionType(1071)

  case object ReversiCheckForReversable extends ActionType(1072)

  case object NonCriticalMasteryLoss extends ActionType(1075)

  case object HpLightLoss extends ActionType(1083)

  case object HpGainLight extends ActionType(1084)

  case object HealsPerformedLoss extends ActionType(1094)

  case object HealsPerformedGain extends ActionType(1095)

  val values = findValues
}
