package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class StorageBox(
  id: Int,
  visualId: Int,
  _2: StorageBox_2,
  compartments: Array[StorageBoxCompartments]
)

@BinaryDecoder
case class StorageBoxCompartments(
  uid: Int,
  boxId: Int,
  unlockItemId: Int,
  capacity: Byte,
  compartmentOrder: Int
)

@BinaryDecoder
case class StorageBox_2(
  _0: Byte,
  _1: Int
)

object StorageBox extends BinaryDataCompanion[StorageBox] {
  override val dataId = 70
}
