package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Recipe(
  id: Int,
  categoryId: Int,
  duration: Long,
  criteria: String,
  visibilityCriteria: String,
  _5: Boolean,
  level: Int,
  properties: Array[Int],
  xpRatio: Int,
  machinesUsingRecipe: Array[Int],
  successRate: Int,
  contractEnabled: Boolean,
  neededKamas: Long,
  xp: Long,
  _14: Int,
  ingredients: Array[RecipeIngredients],
  products: Array[RecipeProducts],
  materials: Array[RecipeMaterials]
)

@BinaryDecoder
case class RecipeMaterials(
  minLevel: Short,
  minRarity: Short,
  optionnal: Boolean,
  materialTypes: Array[Int]
)

@BinaryDecoder
case class RecipeProducts(
  itemId: Int,
  quantity: Short
)

@BinaryDecoder
case class RecipeIngredients(
  itemId: Int,
  quantity: Short,
  _3: Short
)

object Recipe extends BinaryDataCompanion[Recipe] {
  override val dataId = 58
}
