package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class KrosmozGameBoardIeParam(
  id: Int,
  gameId: Byte
)

object KrosmozGameBoardIeParam extends BinaryDataCompanion[KrosmozGameBoardIeParam] {
  override val dataId = 108
}
