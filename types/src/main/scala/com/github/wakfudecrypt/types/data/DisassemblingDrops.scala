package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DisassemblingDrops(
  _0: Short,
  _1: Int
)

object DisassemblingDrops extends BinaryDataCompanion[DisassemblingDrops] {
  override val dataId = 83
}
