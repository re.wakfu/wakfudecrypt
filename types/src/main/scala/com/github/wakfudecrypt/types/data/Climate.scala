package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Climate(
  _0: Int,
  _1: Byte,
  _2: Byte,
  _3: Byte,
  _4: Byte,
  _5: Float,
  _6: Float,
  _7: Int,
  _8: Int,
  _9: Int,
  _10: Int
)

object Climate extends BinaryDataCompanion[Climate] {
  override val dataId = 19
}
