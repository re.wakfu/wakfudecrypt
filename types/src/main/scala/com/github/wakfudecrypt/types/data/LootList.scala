package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class LootList(
  _0: Int,
  _1: Short,
  _2: Array[LootList_2]
)

@BinaryDecoder
case class LootList_2(
  _0: Int,
  _1: Double,
  _2: String,
  _3: Short,
  _4: Short,
  _5: Short,
  _6: Short,
  _7: Short,
  _8: Boolean
)

object LootList extends BinaryDataCompanion[LootList] {
  override val dataId = 39
}
