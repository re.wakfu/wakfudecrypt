package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class NationRank(
  id: Int,
  citizenPointLossFactor: Float,
  criteria: String,
  citizenScoreLine: Int
)

object NationRank extends BinaryDataCompanion[NationRank] {
  override val dataId = 53
}
