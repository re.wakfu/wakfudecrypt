package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class KrosmozGameCollectionIeParam(
  id: Int,
  gameId: Byte
)

object KrosmozGameCollectionIeParam extends BinaryDataCompanion[KrosmozGameCollectionIeParam] {
  override val dataId = 109
}
