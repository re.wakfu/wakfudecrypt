package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BookcaseIeParam(
  id: Int,
  size: Byte,
  _2: Array[Int]
)

object BookcaseIeParam extends BinaryDataCompanion[BookcaseIeParam] {
  override val dataId = 105
}
