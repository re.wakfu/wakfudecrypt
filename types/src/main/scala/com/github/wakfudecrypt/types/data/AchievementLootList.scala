package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AchievementLootList(
  _0: Int,
  _1: Short,
  _2: Array[AchievementLootList_2]
)

@BinaryDecoder
case class AchievementLootList_2(
  _0: Int,
  _1: Double,
  _2: String,
  _3: Int
)

object AchievementLootList extends BinaryDataCompanion[AchievementLootList] {
  override val dataId = 141
}
