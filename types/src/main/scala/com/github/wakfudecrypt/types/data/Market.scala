package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Market(
  _0: Int
)

object Market extends BinaryDataCompanion[Market] {
  override val dataId = 40
}
