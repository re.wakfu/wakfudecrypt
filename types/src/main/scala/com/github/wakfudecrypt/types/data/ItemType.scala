package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ItemType(
  id: Short,
  parentId: Short,
  visibleInAnimations: Boolean,
  visibleInMarketPlace: Boolean,
  recyclable: Boolean,
  equipmentPosition: Array[String],
  disabledEquipementPosition: Array[String],
  materialType: Short,
  craftIds: Array[Int]
)

object ItemType extends BinaryDataCompanion[ItemType] {
  override val dataId = 37
}
