package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldBuildingDecoDefinition(
  id: Int,
  catalogEntryId: Short,
  kamaCost: Int,
  ressourceCost: Int,
  editorGroupId: Int
)

object HavenWorldBuildingDecoDefinition extends BinaryDataCompanion[HavenWorldBuildingDecoDefinition] {
  override val dataId = 126
}
