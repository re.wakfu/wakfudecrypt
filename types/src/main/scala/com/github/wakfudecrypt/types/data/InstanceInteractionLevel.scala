package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class InstanceInteractionLevel(
  id: Int,
  worldId: Int,
  subscriptionLevel: Int,
  interactionLevel: Int
)

object InstanceInteractionLevel extends BinaryDataCompanion[InstanceInteractionLevel] {
  override val dataId = 134
}
