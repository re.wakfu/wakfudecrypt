package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class LightLootList(
  id: Int,
  _1: Array[LightLootList_1]
)

@BinaryDecoder
case class LightLootList_1(
  _0: Int,
  _1: Short,
  _2: Short,
  _3: Short,
  _4: Boolean
)

object LightLootList extends BinaryDataCompanion[LightLootList] {
  override val dataId = 114
}
