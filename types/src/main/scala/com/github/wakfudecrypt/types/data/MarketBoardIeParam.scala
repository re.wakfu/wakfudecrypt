package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MarketBoardIeParam(
  id: Int,
  visualMruId: Int,
  marketId: Int,
  _3: MarketBoardIeParam_3
)

@BinaryDecoder
case class MarketBoardIeParam_3(
  _0: Byte,
  _1: Int
)

object MarketBoardIeParam extends BinaryDataCompanion[MarketBoardIeParam] {
  override val dataId = 41
}
