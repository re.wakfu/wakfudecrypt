package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AlmanachEntry(
  id: Int,
  scenarioId: Int,
  achievementId: Int,
  territories: Array[Int]
)

object AlmanachEntry extends BinaryDataCompanion[AlmanachEntry] {
  override val dataId = 98
}
