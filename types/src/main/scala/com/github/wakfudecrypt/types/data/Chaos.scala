package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Chaos(
  _0: Int,
  _1: Short,
  _2: Short,
  _3: Boolean,
  _4: Byte,
  _5: Byte,
  _6: Byte
)

object Chaos extends BinaryDataCompanion[Chaos] {
  override val dataId = 14
}
