package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Resource(
  id: Int,
  `type`: Int,
  _2: Int,
  idealRainMin: Short,
  idealRainMax: Short,
  _5: Boolean,
  isBlocking: Boolean,
  useBigChallengeAps: Boolean,
  isMonsterEmbryo: Boolean,
  monsterStepHatching: Short,
  properties: Array[Int],
  monsterFamilies: Array[Int],
  steps: Array[ResourceSteps],
  height: Short,
  iconGfxId: Int,
  gfxIds: Map[Int, Array[Int]]
)

@BinaryDecoder
case class ResourceStepsActions(
  id: Int,
  skillId: Int,
  resourceNextIndex: Int,
  skillLevelRequired: Int,
  skillSimultaneousPlayer: Int,
  skillVisualFeedbackId: Int,
  duration: Int,
  consumableId: Int,
  gfxId: Int,
  criteria: String,
  _10: Boolean,
  collectLootListId: Int,
  _12: Float,
  collectItemId: Int,
  lootItems: Array[Int],
  mruOrder: Int,
  displayInCraftDialog: Boolean
)

@BinaryDecoder
case class ResourceSteps(
  index: Int,
  actions: Array[ResourceStepsActions],
  sizeCategoryId: Int,
  lightRadius: Int,
  lightColor: Int,
  apsId: Int,
  apsPosZ: Int
)

object Resource extends BinaryDataCompanion[Resource] {
  override val dataId = 60
}
