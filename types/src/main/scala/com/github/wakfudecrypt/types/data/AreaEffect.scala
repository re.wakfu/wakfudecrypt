package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AreaEffect(
  id: Int,
  scriptId: Int,
  areaAreaId: Int,
  maxExecutionCount: Int,
  targetsToShow: Int,
  canBeTargeted: Boolean,
  obstacleForAI: Boolean,
  shouldStopMovement: Boolean,
  canBeTargetedByAI: Boolean,
  canBeDestroyed: Boolean,
  `type`: String,
  areaAreaParams: Array[Int],
  applicationTriggers: Array[Int],
  unapplicationTriggers: Array[Int],
  destructionTriggers: Array[Int],
  deactivationDelay: Array[Float],
  params: Array[Float],
  properties: Array[Int],
  effectIds: Array[Int],
  areaGfx: String,
  areaCellGfx: String,
  aps: String,
  cellAps: String,
  maxLevel: Int
)

object AreaEffect extends BinaryDataCompanion[AreaEffect] {
  override val dataId = 4
}
