package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CharacGainPerLevel(
  breedId: Short,
  gains: Map[Byte, Float]
)

object CharacGainPerLevel extends BinaryDataCompanion[CharacGainPerLevel] {
  override val dataId = 16
}
