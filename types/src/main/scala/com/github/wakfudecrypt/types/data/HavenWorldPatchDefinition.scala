package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldPatchDefinition(
  id: Int,
  patchId: Int,
  kamaCost: Int,
  categoryId: Int,
  soundId: Int
)

object HavenWorldPatchDefinition extends BinaryDataCompanion[HavenWorldPatchDefinition] {
  override val dataId = 101
}
