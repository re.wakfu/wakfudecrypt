package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CollectIeParam(
  id: Int,
  visualId: Int,
  capacity: Short,
  locked: Boolean,
  cashQty: Int,
  items: Array[CollectIeParamItems],
  actions: Array[CollectIeParamActions]
)

@BinaryDecoder
case class CollectIeParamActions(
  actionId: Int,
  actionType: Int,
  params: Array[String],
  criteria: String
)

@BinaryDecoder
case class CollectIeParamItems(
  uid: Int,
  itemId: Int,
  qty: Int
)

object CollectIeParam extends BinaryDataCompanion[CollectIeParam] {
  override val dataId = 22
}
