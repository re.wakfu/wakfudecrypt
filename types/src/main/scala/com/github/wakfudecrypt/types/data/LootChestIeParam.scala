package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class LootChestIeParam(
  id: Int,
  visualId: Int,
  cooldown: Int,
  cost: Int,
  itemIdCost: Int,
  itemQuantityCost: Int,
  doConsumeItem: Boolean,
  nbActivation: Int,
  _8: Boolean,
  distributionDuration: Int,
  criteria: String,
  _11: LootChestIeParam_11
)

@BinaryDecoder
case class LootChestIeParam_11(
  _0: Byte,
  _1: Int
)

object LootChestIeParam extends BinaryDataCompanion[LootChestIeParam] {
  override val dataId = 38
}
