package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class StoolIeParam(
  id: Int,
  criterion: String,
  visualId: Int,
  _3: StoolIeParam_3
)

@BinaryDecoder
case class StoolIeParam_3(
  _0: Byte,
  _1: Int
)

object StoolIeParam extends BinaryDataCompanion[StoolIeParam] {
  override val dataId = 69
}
