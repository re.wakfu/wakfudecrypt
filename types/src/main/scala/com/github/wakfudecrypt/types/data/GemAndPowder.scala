package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class GemAndPowder(
  id: Int
)

object GemAndPowder extends BinaryDataCompanion[GemAndPowder] {
  override val dataId = 124
}
