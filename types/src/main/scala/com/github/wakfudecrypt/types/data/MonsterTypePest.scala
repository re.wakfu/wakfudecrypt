package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterTypePest(
  id: Int,
  familyId: Int,
  pestMonsterId: Int
)

object MonsterTypePest extends BinaryDataCompanion[MonsterTypePest] {
  override val dataId = 48
}
