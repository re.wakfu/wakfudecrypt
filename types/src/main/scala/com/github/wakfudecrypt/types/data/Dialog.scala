package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Dialog(
  id: Int,
  criteria: String,
  answers: Array[DialogAnswers]
)

@BinaryDecoder
case class DialogAnswers(
  id: Int,
  criterion: String,
  `type`: Int,
  clientOnly: Boolean
)

object Dialog extends BinaryDataCompanion[Dialog] {
  override val dataId = 27
}
