package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ArcadeWaveComposition(
  _0: Int,
  _1: Array[Int]
)

object ArcadeWaveComposition extends BinaryDataCompanion[ArcadeWaveComposition] {
  override val dataId = 91
}
