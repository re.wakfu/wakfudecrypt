package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DisassemblingDropRate(
  _0: Short,
  _1: Double
)

object DisassemblingDropRate extends BinaryDataCompanion[DisassemblingDropRate] {
  override val dataId = 84
}
