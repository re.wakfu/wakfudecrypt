package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class PvpBattleground(
  _0: Int,
  _1: Int,
  _2: Array[PvpBattleground_2],
  _3: Array[PvpBattleground_3]
)

@BinaryDecoder
case class PvpBattleground_3(
  _0: Int,
  _1: Int
)

@BinaryDecoder
case class PvpBattleground_2(
  _0: Int,
  _1: Int,
  _2: Int,
  _3: Int,
  _4: Int,
  _5: Int,
  _6: Int
)

object PvpBattleground extends BinaryDataCompanion[PvpBattleground] {
  override val dataId = 140
}
