package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ResourceType(
  _0: Int,
  _1: Boolean
)

object ResourceType extends BinaryDataCompanion[ResourceType] {
  override val dataId = 61
}
