package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class SkillAction(
  id: Int,
  mruGfxId: Int,
  mruKey: String,
  associatedItems: Array[Int],
  animLinkage: String
)

object SkillAction extends BinaryDataCompanion[SkillAction] {
  override val dataId = 65
}
