package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class NationLaw(
  id: Int,
  lawConstantId: Int,
  params: Array[String],
  basePointsModification: Int,
  percentPointsModification: Int,
  lawPointCost: Int,
  lawLocked: Boolean,
  applicableToCitizen: Boolean,
  applicableToAlliedForeigner: Boolean,
  applicableToNeutralForeigner: Boolean,
  restrictedNations: Array[Int]
)

object NationLaw extends BinaryDataCompanion[NationLaw] {
  override val dataId = 52
}
