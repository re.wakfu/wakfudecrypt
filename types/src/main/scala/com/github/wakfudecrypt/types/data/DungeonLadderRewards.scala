package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DungeonLadderRewards(
  _0: Int,
  _1: Short,
  _2: Short,
  _3: Int,
  _4: Boolean
)

object DungeonLadderRewards extends BinaryDataCompanion[DungeonLadderRewards] {
  override val dataId = 142
}
