package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BoatLink(
  id: Int,
  start: Int,
  end: Int,
  cost: Int,
  criteria: String,
  criteriaDisplay: String,
  needsToPayEverytime: Boolean,
  _7: BoatLink_7
)

@BinaryDecoder
case class BoatLink_7(
  _0: String,
  _1: Int,
  _2: Int,
  _3: Int
)

object BoatLink extends BinaryDataCompanion[BoatLink] {
  override val dataId = 9
}
