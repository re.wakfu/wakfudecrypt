package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class InteractiveElementModel(
  id: Int,
  `type`: Short,
  gfx: Int,
  color: Int,
  height: Byte,
  particleId: Int,
  particleOffsetZ: Int
)

object InteractiveElementModel extends BinaryDataCompanion[InteractiveElementModel] {
  override val dataId = 34
}
