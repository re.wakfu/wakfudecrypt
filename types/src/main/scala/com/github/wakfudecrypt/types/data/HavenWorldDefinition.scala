package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldDefinition(
  id: Int,
  worldId: Short,
  workers: Byte,
  exitWorldId: Short,
  exitCellX: Short,
  exitCellY: Short,
  exitCellZ: Short
)

object HavenWorldDefinition extends BinaryDataCompanion[HavenWorldDefinition] {
  override val dataId = 100
}
