package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class FightChallengeMonster(
  id: Int,
  randomRolls: Short,
  forcedRolls: Short,
  forcedChallenges: Array[Int]
)

object FightChallengeMonster extends BinaryDataCompanion[FightChallengeMonster] {
  override val dataId = 132
}
