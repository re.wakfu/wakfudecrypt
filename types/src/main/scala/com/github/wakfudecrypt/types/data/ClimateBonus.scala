package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ClimateBonus(
  buffId: Int,
  gfxId: Int,
  criteria: String,
  duration: Int,
  price: Short,
  temperatureDifference: Float,
  rainDifference: Float
)

object ClimateBonus extends BinaryDataCompanion[ClimateBonus] {
  override val dataId = 20
}
