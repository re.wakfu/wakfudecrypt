package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Treasure(
  id: Int,
  usedItem: Int,
  rewardItem: Int,
  quantity: Short,
  rewardKama: Int,
  rewardLootChest: Int,
  duration: Int,
  criterion: String,
  winPercent: Float
)

object Treasure extends BinaryDataCompanion[Treasure] {
  override val dataId = 122
}
