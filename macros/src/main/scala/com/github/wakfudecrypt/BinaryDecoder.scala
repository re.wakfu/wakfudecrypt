package com.github.wakfudecrypt

import scala.annotation.compileTimeOnly
import scala.meta._
import scala.collection.immutable.Seq

@compileTimeOnly("@BinaryDecoder not expanded")
class BinaryDecoder extends scala.annotation.StaticAnnotation {
   inline def apply(defn: Any): Any = meta {
    def createDecoder(name: Type.Name,
                      paramss: Seq[Seq[Term.Param]]): Defn.Val = {

      val args = paramss.map(_.map( p =>
        q"implicitly[Decoder[${p.decltpe.get.asInstanceOf[Type]}]].decode(rb)"
      ))
      q"""implicit val ${Pat.fresh("decoder")} = new _root_.com.github.wakfudecrypt.Decoder[$name] {
            import com.github.wakfudecrypt._
            @inline final def decode(rb: RandomizedBuffer): $name = {
              import com.github.wakfudecrypt.codecs._
              ${Term.Name(name.value)}(...$args)
            }
        }"""
    }
    defn match {
      case Term.Block(
      Seq(cls @ Defn.Class(_, name, _, ctor, _),
      companion: Defn.Object)) =>
        val applyMethod = createDecoder(name, ctor.paramss)
        val templateStats: Seq[Stat] =
          applyMethod +: companion.templ.stats.getOrElse(Nil)
        val newCompanion = companion.copy(
          templ = companion.templ.copy(stats = Some(templateStats)))
        Term.Block(Seq(cls, newCompanion))
      // companion object does not exists
      case cls @ Defn.Class(_, name, _, ctor, _) =>
        val applyMethod = createDecoder(name, ctor.paramss)
        val companion   = q"object ${Term.Name(name.value)} { $applyMethod }"
        Term.Block(Seq(cls, companion))
      case _ =>
        println(defn.structure)
        abort("@BinaryDecoder must annotate a case class.")
    }
  }
}