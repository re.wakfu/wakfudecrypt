package com.github.wakfudecrypt

import scala.reflect.ClassTag

/** Implicit codecs for basic types */
object codecs {
  implicit val byteDecoder = new Decoder[Byte] {
    def decode(b: RandomizedBuffer): Byte = b.getByte
  }

  implicit val boolDecoder = new Decoder[Boolean] {
    def decode(b: RandomizedBuffer): Boolean = b.getBool
  }

  implicit val shortDecoder = new Decoder[Short] {
    def decode(b: RandomizedBuffer): Short = b.getShort
  }

  implicit val floatDecoder = new Decoder[Float] {
    def decode(b: RandomizedBuffer): Float = b.getFloat
  }

  implicit val intDecoder = new Decoder[Int] {
    def decode(b: RandomizedBuffer): Int = b.getInt
  }

  implicit val doubleDecoder = new Decoder[Double] {
    def decode(b: RandomizedBuffer): Double = b.getDouble
  }

  implicit val longDecoder = new Decoder[Long] {
    def decode(b: RandomizedBuffer): Long = b.getLong
  }

  implicit val UTF8Decoder = new Decoder[String] {
    def decode(b: RandomizedBuffer): String = b.getUTF8
  }

  implicit def arrayDecoder[T: Decoder: ClassTag] = new Decoder[Array[T]] {
    def decode(b: RandomizedBuffer): Array[T] =
      Array.fill(b.getInt)(implicitly[Decoder[T]].decode(b))
  }

  implicit def mapDecoder[K: Decoder, V: Decoder] = new Decoder[Map[K, V]] {
    def decode(b: RandomizedBuffer): Map[K, V] =
      Map(Seq.fill(b.getInt)(implicitly[Decoder[K]].decode(b) → implicitly[Decoder[V]].decode(b)): _*)
  }
}
