package com.github.wakfudecrypt

trait HasBinaryDataId[T] {
  def id: Int
}

trait BinaryDataCompanion[T] {
  def dataId: Int

  implicit def instance: HasBinaryDataId[T] =
    new HasBinaryDataId[T] {
      override def id: Int = dataId
    }
}
