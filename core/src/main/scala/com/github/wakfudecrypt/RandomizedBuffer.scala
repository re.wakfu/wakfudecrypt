package com.github.wakfudecrypt

import java.nio.ByteBuffer
import scala.language.postfixOps

class RandomizedBuffer(bb: ByteBuffer, val mul: Int, val add: Int) extends Randomizer {
  @inline final def getPosition: Int = bb.position

  @inline final def setPosition(pos: Int): Unit = bb.position(pos)

  @inline final def getByte: Byte = { inc(); bb.get - seed toByte }

  @inline final def getBool: Boolean = { inc(); bb.get - seed != 0 }

  @inline final def getShort: Short = { inc(); bb.getShort - seed toShort }

  @inline final def getFloat: Float = { inc(); bb.getFloat }

  @inline final def getInt: Int = { inc(); bb.getInt - seed }

  @inline final def getDouble: Double = { inc(); bb.getDouble }

  @inline final def getLong: Long = { inc(); bb.getLong - seed }

  final def getUTF8: String = {
    val data = new Array[Byte](getInt)
    bb.get(data)
    new String(data, "UTF-8")
  }
}